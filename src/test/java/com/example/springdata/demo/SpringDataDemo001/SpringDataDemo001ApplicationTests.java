package com.example.springdata.demo.SpringDataDemo001;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springdata.demo.SpringDataDemo001.model.Book;
import com.example.springdata.demo.SpringDataDemo001.repository.BookRepository;
import com.example.springdata.demo.SpringDataDemo001.service.BookRentalService;
import com.example.springdata.demo.SpringDataDemo001.service.BookRentalserviceImpl;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
//@DataJpaTest
@Slf4j
public class SpringDataDemo001ApplicationTests {

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private BookRentalserviceImpl bookRentalserviceImpl;
	
	@Test
	public void contextLoads() {
	}
	
	public void testingFindByTitle() {
		Book myAwesomeBook = new Book();
		myAwesomeBook.setNumberOfPages(570);
		myAwesomeBook.setAuthor("Vasiiliki");
		myAwesomeBook.setTitle("the book");
		bookRepository.save(myAwesomeBook);
		
		//Testing findbyTitle
		log.info("Testing find by title for book");
		Book book = bookRepository.findBookByTitle("Forever Vivlio");
		log.info("BOOK FOUND : {}", book.toString());
	}

}
