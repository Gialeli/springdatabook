package com.example.springdata.demo.SpringDataDemo001.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.example.springdata.demo.SpringDataDemo001.model.Book;
import com.example.springdata.demo.SpringDataDemo001.repository.BookRepository;

@Service
public class DBInit implements ApplicationListener<ApplicationEvent>{

	@Autowired
	private BookRepository bookRepository;
	
	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		Book myAwesomeBook = new Book();
		myAwesomeBook.setNumberOfPages(570);
		myAwesomeBook.setAuthor("Vasiiliki");
		myAwesomeBook.setTitle("the book");
		bookRepository.save(myAwesomeBook);
	}

}
