package com.example.springdata.demo.SpringDataDemo001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataDemo001Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataDemo001Application.class, args);
	}

}
