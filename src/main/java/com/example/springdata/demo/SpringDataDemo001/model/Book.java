package com.example.springdata.demo.SpringDataDemo001.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Entity
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Setter()
	private long id;
	//@Column(nullable = false)
	private int numberOfPages;
	//@Column(unique = true, nullable = false)
	private String title;
	private String author;

}
