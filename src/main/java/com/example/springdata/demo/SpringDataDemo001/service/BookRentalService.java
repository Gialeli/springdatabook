package com.example.springdata.demo.SpringDataDemo001.service;

import java.util.List;

import com.example.springdata.demo.SpringDataDemo001.model.Book;

public interface BookRentalService {

	List<Book> getAllBooks();
	String getBookAuthorByBookTitle(String title);
	
}
