package com.example.springdata.demo.SpringDataDemo001.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springdata.demo.SpringDataDemo001.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	
	

}
